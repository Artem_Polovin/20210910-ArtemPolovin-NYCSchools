package com.example.data.mappers

import com.example.data.api_models.sap_model.SAPDataApiModel
import com.example.data.api_models.schools.SchoolsDataModelApi
import com.example.domain.models.SchoolDataModel

class SchoolsApiMapper {

    fun mapSchoolsDataApiToModel(
        schoolsDtaApi: SchoolsDataModelApi,
        sapDataModel: SAPDataApiModel
    ): List<SchoolDataModel> {

        val schoolList = mutableListOf<SchoolDataModel>()

        schoolsDtaApi.forEach { schoolData ->
            sapDataModel.forEach { sapData ->
                if (schoolData.dbn == sapData.dbn) {
                    schoolList.add(
                        SchoolDataModel(
                            schoolName = schoolData.school_name,
                            academicOpportunities = schoolData.academicopportunities2,
                            location = schoolData.location.substringBefore("("),
                            phoneNumber = schoolData.phone_number,
                            faxNumber = schoolData.fax_number,
                            email = schoolData.school_email,
                            website = schoolData.website,
                            totalStudents = schoolData.total_students,
                            city = schoolData.city,
                            stateCode = schoolData.state_code,
                            schoolId = schoolData.dbn,
                            mathScore = sapData.sat_math_avg_score,
                            readingScore = sapData.sat_critical_reading_avg_score,
                            writingScore = sapData.sat_writing_avg_score
                        )
                    )
                }

            }
        }
        return schoolList
    }
}