package com.example.data.api_models.schools

class SchoolsDataModelApiItem(
    val academicopportunities2: String,
    val city: String,
    val dbn: String,
    val fax_number: String,
    val location: String,
    val school_email: String,
    val school_name: String,
    val state_code: String,
    val total_students: String,
    val website: String,
    val phone_number: String
)