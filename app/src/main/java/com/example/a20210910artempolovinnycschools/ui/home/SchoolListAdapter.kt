package com.example.a20210910artempolovinnycschools.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.example.a20210910artempolovinnycschools.R
import com.example.domain.models.SchoolDataModel
import kotlinx.android.synthetic.main.cell_school.view.*

class SchoolListAdapter: RecyclerView.Adapter<SchoolListAdapter.SchoolsViewHolder>() {

    private val schoolsList = mutableListOf<SchoolDataModel>()

    private val _schoolModel = MutableLiveData<SchoolDataModel>()
    val schoolModel: LiveData<SchoolDataModel>
        get() = _schoolModel

    fun setupList(newList: List<SchoolDataModel>) {
        schoolsList.clear()
        schoolsList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_school, parent, false)
        return SchoolsViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        val schoolModel = schoolsList[position]

        holder.bind(schoolModel)
        holder.onClickItem(schoolModel)
    }

    override fun getItemCount(): Int {
        return schoolsList.size
    }

    inner class SchoolsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(schoolModel: SchoolDataModel) {
            itemView.apply {
                text_school_name.text = schoolModel.schoolName
                text_city_name.text = "${schoolModel.city},"
                text_state.text = schoolModel.stateCode
            }
        }

        fun onClickItem(schoolModel: SchoolDataModel) {
            itemView.setOnClickListener {
                _schoolModel.value = schoolModel
            }
        }
    }
}