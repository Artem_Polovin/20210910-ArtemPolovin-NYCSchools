package com.example.domain.repositories

import com.example.domain.models.SchoolDataModel
import com.example.domain.utils.ResponseResult

interface SchoolRepository {

    suspend fun getSchoolsDataList(): ResponseResult<List<SchoolDataModel>>
}