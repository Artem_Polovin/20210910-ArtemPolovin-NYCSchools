package com.example.data.network.api

import com.example.data.api_models.sap_model.SAPDataApiModel
import com.example.data.api_models.schools.SchoolsDataModelApi
import com.example.data.utils.BASE_SCHOOLS_DATA_URL
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface SchoolsApiService {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchoolList(): Response<SchoolsDataModelApi>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSAPData(): Response<SAPDataApiModel>

    companion object {
        operator fun invoke(): SchoolsApiService {
            return Retrofit.Builder()
                .baseUrl(BASE_SCHOOLS_DATA_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SchoolsApiService::class.java)
        }
    }
}