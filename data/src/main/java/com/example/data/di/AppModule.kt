package com.example.data.di

import com.example.data.mappers.SchoolsApiMapper
import com.example.data.network.api.SchoolsApiService
import com.example.data.repositories_impl.SchoolRepositoryImpl
import com.example.domain.repositories.SchoolRepository
import com.example.domain.usecases.GetSchoolListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideSchoolRepository(
        schoolApi: SchoolsApiService,
        schoolsApiMapper: SchoolsApiMapper
    ): SchoolRepository = SchoolRepositoryImpl(schoolApi, schoolsApiMapper)

    @Provides
    @Singleton
    fun provideSchoolApiService() = SchoolsApiService()

    @Provides
    fun provideSchoolApiMapper() = SchoolsApiMapper()

    @Provides
    fun provideGetSchoolListUseCase(schoolRepo: SchoolRepository) = GetSchoolListUseCase(schoolRepo)

}