package com.example.a20210910artempolovinnycschools.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20210910artempolovinnycschools.R
import com.example.a20210910artempolovinnycschools.utils.MINIMUM_SYMBOLS
import com.example.a20210910artempolovinnycschools.utils.putKSerializable
import com.example.domain.models.SchoolDataModel
import com.example.domain.utils.ResponseResult
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_schools_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SchoolsListFragment : Fragment() {

    private val viewModel: SchoolListViewModel by viewModels()

    private lateinit var mAdapter: SchoolListAdapter

    private val schoolList = mutableListOf<SchoolDataModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schools_list, container, false)
    }

    @FlowPreview
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        mAdapter = SchoolListAdapter()

        btn_retry.setOnClickListener { viewModel.refreshSchoolList() }

        viewModel.refreshSchoolList()

        refreshLayout()
        setupRecyclerView()
        setUpSchoolList()
        openSchoolDetailsScreen()
        inputResultOfSchoolSearching()
        setupFilteredSchoolList()

    }

    private fun setUpSchoolList() {

        viewModel.schoolsList.observe(viewLifecycleOwner) {
            group_hidden_views.visibility = View.GONE
            refresh_layout.isRefreshing = false

            when (it) {
                ResponseResult.Loading -> {
                    refresh_layout.isRefreshing = true
                }
                is ResponseResult.Failure -> {
                    text_error.visibility = View.VISIBLE
                    text_error.text = it.message
                    btn_retry.visibility = View.VISIBLE
                }
                is ResponseResult.Success -> {
                    rv_schools.visibility = View.VISIBLE
                    if (!viewModel.isFiltered) {

                        mAdapter.setupList(it.data)
                        schoolList.clear()
                        schoolList.addAll(it.data)
                        viewModel.receiveSchoolList(schoolList)
                    }
                }
            }
        }
    }

    private fun setupFilteredSchoolList() {
        viewModel.filteredSchoolList.observe(viewLifecycleOwner) {
            if (viewModel.isFiltered) mAdapter.setupList(it)
        }
    }

    private fun openSchoolDetailsScreen() {
        mAdapter.schoolModel.observe(viewLifecycleOwner) { schoolModel ->
            val bundle = Bundle()
            bundle.putKSerializable("schoolModel", schoolModel)
            findNavController().navigate(
                R.id.action_schoolsListFragment_to_schoolDetailsFragment,
                bundle
            )
        }
    }

    private fun setupRecyclerView() {
        rv_schools.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
        }
    }

    private fun resultFromSearchView(): StateFlow<String> {
        val query = MutableStateFlow("")

        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { query.value = it }
                return true
            }

        })
        return query
    }

    @FlowPreview
    private fun inputResultOfSchoolSearching() {

        viewLifecycleOwner.lifecycleScope.launch {
            resultFromSearchView()
                .debounce(500)
                .filter { it.length >= MINIMUM_SYMBOLS }
                .distinctUntilChanged()
                .flowOn(Dispatchers.Default)
                .collect { result ->
                    viewModel.filterSchoolListByCity(result)
                }
        }

    }

    private fun refreshLayout() {
        refresh_layout.setOnRefreshListener {
            viewModel.refreshSchoolList()
        }
    }

}