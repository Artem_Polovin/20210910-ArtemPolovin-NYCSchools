package com.example.domain.usecases

import com.example.domain.repositories.SchoolRepository

class GetSchoolListUseCase(private val schoolRepo: SchoolRepository) {
    suspend fun execute() = schoolRepo.getSchoolsDataList()
}