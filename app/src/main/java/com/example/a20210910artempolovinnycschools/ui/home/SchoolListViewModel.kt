package com.example.a20210910artempolovinnycschools.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.data.api_models.schools.SchoolsDataModelApi
import com.example.domain.models.SchoolDataModel
import com.example.domain.usecases.GetSchoolListUseCase
import com.example.domain.utils.ResponseResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val getSchoolListUseCase: GetSchoolListUseCase
) : ViewModel() {

    var isFiltered = false
    private val schoolList = mutableListOf<SchoolDataModel>()

    private val _schoolsList = MutableLiveData<ResponseResult<List<SchoolDataModel>>>()
    val schoolsList: LiveData<ResponseResult<List<SchoolDataModel>>> get() = _schoolsList

    private val _filteredSchoolList = MutableLiveData<List<SchoolDataModel>>()
    val filteredSchoolList: LiveData<List<SchoolDataModel>>get() = _filteredSchoolList

    private fun fetchSchoolList() {
        viewModelScope.launch {
            isFiltered = false

            _schoolsList.value = ResponseResult.Loading
            _schoolsList.value = getSchoolListUseCase.execute()
        }
    }

    fun filterSchoolListByCity(inputString: String) {
        if (schoolList.isNotEmpty()) {
            isFiltered = true

            _filteredSchoolList.value = schoolList.filter { schoolModel ->
                val schoolCity = schoolModel.city
                schoolCity?.contains(inputString, true) ?: false
            }
        }
    }

    fun refreshSchoolList() {
        fetchSchoolList()
    }

    fun receiveSchoolList(listOfSchools: List<SchoolDataModel>) {
        if (listOfSchools.isNotEmpty()) {
            schoolList.clear()
            schoolList.addAll(listOfSchools)
        }
    }

}