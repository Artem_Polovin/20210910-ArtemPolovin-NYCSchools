package com.example.data.repositories_impl

import com.example.data.api_models.sap_model.SAPDataApiModel
import com.example.data.mappers.SchoolsApiMapper
import com.example.data.network.api.SchoolsApiService
import com.example.domain.models.SchoolDataModel
import com.example.domain.repositories.SchoolRepository
import com.example.domain.utils.ResponseResult
import retrofit2.HttpException
import java.io.IOException

class SchoolRepositoryImpl(
    private val schoolApi: SchoolsApiService,
    private val schoolApiMapper: SchoolsApiMapper
) : SchoolRepository {

    override suspend fun getSchoolsDataList(): ResponseResult<List<SchoolDataModel>> {

        return try {
            val response = schoolApi.getSchoolList()
            if (response.isSuccessful) {
                response.body()?.let {
                    val sapList = getSAPData()
                    return@let ResponseResult.Success(schoolApiMapper.mapSchoolsDataApiToModel(it,sapList))
                } ?: ResponseResult.Failure(message = "An unknown error was accured")
            } else {
                ResponseResult.Failure(message = "The response is not success")
            }
        } catch (e: IOException) {
            e.printStackTrace()
            ResponseResult.Failure(e, message = "Please check connection to internet")
        } catch (e: HttpException) {
            e.printStackTrace()
            ResponseResult.Failure(e, message = "Please check connection to internet")
        }
    }

    private suspend fun getSAPData(): SAPDataApiModel {
        return try {
            val response = schoolApi.getSAPData()
            if (response.isSuccessful) {
                response.body()?.let {
                    return@let it
                } ?: throw  IllegalArgumentException("An unknown error occured")
            } else {
                throw  IllegalArgumentException("${response.errorBody()?.string()}")
            }
        } catch (e: IOException) {
            e.printStackTrace()
            throw  IllegalArgumentException("Please check the internet connection")
        } catch (e: HttpException) {
            e.printStackTrace()
            throw  IllegalArgumentException("Please check the internet connection")
        }
    }
}