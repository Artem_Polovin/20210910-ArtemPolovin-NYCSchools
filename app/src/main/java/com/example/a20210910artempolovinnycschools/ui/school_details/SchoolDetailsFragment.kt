package com.example.a20210910artempolovinnycschools.ui.school_details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.a20210910artempolovinnycschools.R
import com.example.a20210910artempolovinnycschools.utils.getKSerializable
import com.example.domain.models.SchoolDataModel
import kotlinx.android.synthetic.main.fragment_school_details.*
import java.io.IOException

class SchoolDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_school_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val schoolModel = arguments?.getKSerializable<SchoolDataModel>("schoolModel")
        schoolModel?.let { setupSchoolInfoPage(schoolModel) }

        openWebPage()

    }

    private fun setupSchoolInfoPage(schoolModel: SchoolDataModel) {
        schoolModel.apply {
            text_school_name.text = schoolName
            text_academic_opportunities.text = academicOpportunities
            text_total_students.text = totalStudents
            text_math_score.text = mathScore
            text_reading_score.text = readingScore
            text_writing_score.text = writingScore
            text_location.text = location
            text_phone_number.text = phoneNumber
            text_fax_number.text = faxNumber
            text_email.text = email
            text_website.text = website
        }
    }

    private fun openWebPage() {
        text_website.setOnClickListener {
            try {
                val url = Uri.parse("http://${text_website.text}")
                val intent = Intent(Intent.ACTION_VIEW, url)
                activity?.startActivity(intent)
            } catch (e: IOException) {
                e.printStackTrace()
                Toast.makeText(requireContext(), "web page was not found", Toast.LENGTH_LONG).show()
            }
        }
    }
}