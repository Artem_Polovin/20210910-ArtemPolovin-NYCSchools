package com.example.domain.models

import kotlinx.serialization.Serializable

@Serializable
data class SchoolDataModel(
    val schoolName: String?,
    val academicOpportunities: String?,
    val location: String?,
    val phoneNumber: String?,
    val faxNumber: String?,
    val email: String?,
    val website: String?,
    val totalStudents: String?,
    val city: String?,
    val stateCode: String?,
    val mathScore: String?,
    val readingScore: String?,
    val writingScore: String?,
    val schoolId: String
)